<?php
		
	/** DATABASE CONFIGURATION */
	$dbConfig = array(
			"username" => "root",
			"password" => "",
			"database" => "carsale"
		);

	$con = mysqli_connect("localhost", $dbConfig["username"], $dbConfig["password"]); // Configure host, database username, database password here

	$db = mysqli_select_db($con, $dbConfig["database"]); // CONFIGURE DATABSE HERE

	function prepareModel($internalHtml) {
		$array = array();

			$title = $internalHtml->find('h1[class=details-title]');

			if(count($title) > 0) {
				$titleParts = explode(' ', trim($title[0]->innertext));

				$array["make"] = $titleParts[1];

				$model = array_splice($titleParts, 2);

				$model = implode(' ', $model);

				$model = trim(strip_tags(preg_replace('/(MY[0-9]{1,2}\.[0-9]{0,2})|(MY[0-9]{1,2})|(Manufacturer(.*)\))|(The manufacturer(.*)model.)|(\s){2,}|(\n){1,}|(\r\n){1,}/', '', $model)));

				$array['model'] = $model;

			    $price =  $internalHtml->find('div[class=price-value]');
			    if(count($price) > 0) {
			    	$array['price'] = $price[0]->innertext; 
			    }

			    $locationEl = $internalHtml->find('div[class=seller-details]');
			    if(count($locationEl) > 0) {
			    	$location = $locationEl[0]->children(0)->children(1)->children(1)->children(1)->innertext;
			    	$array['location'] = $location;
			    } else {
			    	$array['location'] = '';
			    }

			    $detailsTable = $internalHtml->find('table[class=vertical]');
			    
			    for ($i = 0; $i < count($detailsTable[0]->children(0)->children); $i++) { 
			   		if($detailsTable[0]->children(0)->children($i)->children(0)->innertext == "Colour") {
			    		$array['colour'] = trim($detailsTable[0]->children(0)->children($i)->children(1)->innertext);
			   		}
			   		if($detailsTable[0]->children(0)->children($i)->children(0)->innertext == "Kilometres") {
			    		$array['kilometres'] = trim($detailsTable[0]->children(0)->children($i)->children(1)->innertext);
			   		}
			   		if($detailsTable[0]->children(0)->children($i)->children(0)->innertext == "Engine") {
			    		$array['engineSize'] = trim($detailsTable[0]->children(0)->children($i)->children(1)->innertext);
			   		}
			   		if($detailsTable[0]->children(0)->children($i)->children(0)->innertext == "Drive Type") {
			    		$array['driveType'] = trim($detailsTable[0]->children(0)->children($i)->children(1)->innertext);
			   		}
			   		if($detailsTable[0]->children(0)->children($i)->children(0)->innertext == "Transmission") {
			    		$array['transmission'] = trim($detailsTable[0]->children(0)->children($i)->children(1)->innertext);
			   		}
			   		if($detailsTable[0]->children(0)->children($i)->children(0)->innertext == "Towing Braked") {
			    		$array['towing'] = trim($detailsTable[0]->children(0)->children($i)->children(1)->innertext);
			   		}
			   	}

			    $mediaHeadings = $internalHtml->find('span[class=media-heading]');
			    
			    $array['body'] = $mediaHeadings[1]->innertext;

				$specsEl = $internalHtml->getElementById("features-Specifications")->children(1)->children;

				for ($i = 0; $i < count($specsEl); $i++) { 
					if(trim($specsEl[$i]->children(0)->plaintext) == "Engine") {
						$sub = $specsEl[$i]->children(1)->children(0)->children;

						for ($j = 0; $j < count($sub); $j++) { 

							$head = trim($sub[$j]->children(0)->children(0)->plaintext);

							if($head == "Cylinders") {
								$array['cylinders'] = trim($sub[$j]->children(1)->plaintext);
							}
							
							if($head == "Power") {
								$array['power'] = trim($sub[$j]->children(1)->plaintext);
							}
							
							if($head == "Induction") {
								$array['inductionTurbo'] = trim($sub[$j]->children(1)->plaintext);
							}
						}
					}

					if(trim($specsEl[$i]->children(0)->plaintext) == "Fuel") {
						$sub = $specsEl[$i]->children(1)->children(0)->children;

						for ($j = 0; $j < count($sub); $j++) { 

							$head = trim($sub[$j]->children(0)->children(0)->plaintext);

							if($head == "Fuel Type") {
								$array['fuelType'] = trim($sub[$j]->children(1)->plaintext);
							}
						}
					}

					if(trim($specsEl[$i]->children(0)->plaintext) == "Other") {
						$sub = $specsEl[$i]->children(1)->children(0)->children;

						for ($j = 0; $j < count($sub); $j++) { 

							$head = trim($sub[$j]->children(0)->children(0)->plaintext);

							if($head == "Launch Year") {
								$array['year'] = trim($sub[$j]->children(1)->plaintext);
							}

							if($head == "Series") {
								$array['series'] = trim($sub[$j]->children(1)->plaintext);
							}

							if($head == "Badge") {
								$array['badge'] = trim($sub[$j]->children(1)->plaintext);
							}

							if($head == "Doors") {
								$array['doors'] = trim($sub[$j]->children(1)->plaintext);
							}

							if($head == "Seat Capacity") {
								$array['seats'] = trim($sub[$j]->children(1)->plaintext);
							}
						}
					}
				}

			    // $specs1 = $internalHtml->getElementById('features-Specifications-1');
			    
			    // if($specs1) {
			    // 	$array['cylinders'] = trim($specs1->children(0)->children(5)->children(1)->children(0)->innertext);
			    // 	$array['power'] = trim($specs1->children(0)->children(10)->children(1)->children(0)->innertext);
			    // 	$array['inductionTurbo'] = trim($specs1->children(0)->children(3)->children(1)->children(0)->innertext);
			    // }
			     
			    // $specs3 = $internalHtml->getElementById('features-Specifications-3');
			    // if($specs3) {
			    // 	$node1 = trim($specs3->children(0)->children(0)->children(0)->children(0)->innertext);
			    // 	$node2 = trim($specs3->children(0)->children(1)->children(0)->children(0)->innertext);
			    // 	if($node1 == "Fuel Type") {
			    // 		$array['fuelType'] = trim($specs3->children(0)->children(0)->children(1)->children(0)->innertext);
			    // 	} else {
			    // 		$array['fuelType'] = trim($specs3->children(0)->children(1)->children(1)->children(0)->innertext);
			    // 	}
			    // }

			   	
			   	// $specs8 = $internalHtml->getElementById('features-Specifications-8');
			   	// if($specs8) {
			   	// 	for ($i=0; $i < count($specs8->children(0)->children); $i++) {
			   	// 		if(preg_match('/Launch Year/', $specs8->children(0)->children($i)->children(0)->children(0)->plaintext)) {
			    // 			$array['year'] = trim($specs8->children(0)->children($i)->children(1)->children(0)->plaintext);
			   	// 		}
			   	// 		if(preg_match('/Series/', $specs8->children(0)->children($i)->children(0)->children(0)->plaintext)) {
			    // 			$array['series'] = trim($specs8->children(0)->children($i)->children(1)->children(0)->plaintext);
			   	// 		}
			   	// 		if(preg_match('/Badge/', $specs8->children(0)->children($i)->children(0)->children(0)->plaintext)) {
			    // 			$array['badge'] = trim($specs8->children(0)->children($i)->children(1)->children(0)->plaintext);
			   	// 		}
			   	// 		if($specs8->children(0)->children($i)->children(0)->children(0)->innertext == "Doors") {
			    // 			$array['doors'] = trim($specs8->children(0)->children($i)->children(1)->children(0)->plaintext);
			   	// 		}
			   	// 		if( preg_match('/Seat Capacity/', $specs8->children(0)->children($i)->children(0)->children(0)->plaintext)) {
			    // 			$array['seats'] = trim($specs8->children(0)->children($i)->children(1)->children(0)->plaintext);
			   	// 		}
			   	// 	}
			   	// }

			   	$array['year'] = isset($array['year']) ? $array['year'] : '';
			   	$array['series'] = isset($array['series']) ? $array['series'] : '';
			   	$array['badge'] = isset($array['badge']) ? $array['badge'] : '';
			   	$array['doors'] = isset($array['doors']) ? $array['doors'] : '';
			   	$array['colour'] = isset($array['colour']) ? $array['colour'] : '';
			   	$array['kilometres'] = isset($array['kilometres']) ? $array['kilometres'] : '';
			   	$array['engineSize'] = isset($array['engineSize']) ? $array['engineSize'] : '';
			   	$array['driveType'] = isset($array['driveType']) ? $array['driveType'] : '';
			   	$array['transmission'] = isset($array['transmission']) ? $array['transmission'] : '';
			   	$array['towing'] = isset($array['towing']) ? $array['towing'] : '';

		    	return $array;
			} else {
				return 2;
			}


	}

	function insertRec($data) {
		global $db;
		global $con;

		$query = mysqli_query($con, "SELECT COUNT(id) as count FROM data WHERE url = '" . $data['url'] . "'");
		
		if(!$query) {
			die("ERROR > " . mysqli_error($con));
		}

		$row = mysqli_fetch_array($query, MYSQLI_NUM);

		if($row[0] === 1) {
			return;
		}

		$query = "INSERT INTO data (
				make,
			    model,
			    year,
			    price,
			    location,
			    transmission,
			    driveType,
			    cylinders,
			    power,
			    inductionTurbo,
			    fuelType,
			    kilometres,
			    engineSize,
			    towing,
			    body,
			    seats,
			    doors,
			    colour,
			    url
			) VALUES (
				'".$data['make']."',
			    '".$data['model']."',
			    '".$data['year']."',
			    '".$data['price']."',
			    '".$data['location']."',
			    '".$data['transmission']."',
			    '".$data['driveType']."',
			    '".$data['cylinders']."',
			    '".$data['power']."',
			    '".$data['inductionTurbo']."',
			    '".$data['fuelType']."',
			    '".$data['kilometres']."',
			    '".$data['engineSize']."',
			    '".$data['towing']."',
			    '".$data['body']."',
			    '".$data['seats']."',
			    '".$data['doors']."',
			    '".$data['colour']."', 
			    '".$data['url']."'
			)";

		// echo "\n". $query . "\n\n";

		$query = mysqli_query($con, $query);

		if(!$query) {
			die("ERROR > " . mysqli_error($con));
		}
	} 
?>