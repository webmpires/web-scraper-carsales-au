<?php 
	
	/** Load HTML DOM Parser */
	include("simple_html_dom.php");

	/** MYSQL Connect */
	include("db.php");

	function getHtmlByPage($page) {
		/** SET FILTER PARAMS */
		$params = array(
			"offset" => (($page - 1) * 12),
			"setype" => "pagination",
			"limit" => 24,
			"q" => "Service.Carsales.",
			"area" => "Stock",
			"vertical" => "car",
			"WT.z_srchsrcx" => "makemodel"
		);

		$queryString = '';

		foreach ($params as $key => $value) {
			$queryString .= $key . "=" . $value . "&";
		}

		$queryString = rtrim($queryString, '&');

		$html = file_get_html("https://www.carsales.com.au/cars/results?" . $queryString);

		return $html;
	}

	/** Get number of pages */
	$tmpHtml = getHtmlByPage(1);

	$pagesEl = $tmpHtml->find('div[class=pagination]');
	$pages = preg_replace('/\D/', '', $pagesEl[0]->children(0)->plaintext);

	for ($i = 1; $i <= $pages; $i++) {

		$html = getHtmlByPage($i);

		$ret = $html->find('div[class=n_width-max title]');

		if(count($ret) > 0) {
			// Capture all links on page
			foreach ($ret as $el) {

				$link = "https://www.carsales.com.au" . $el->children(0)->getAttribute('href');
				// echo $link . "\n";

				$internalHtml = file_get_html($link);

				if($internalHtml) {

					// Prepare data model to be insert 
					$data = prepareModel($internalHtml);
					// echo "<pre>";
					// print_r($data);
					if(is_numeric($data) && $data == 2) {
						echo "Could not get data from " . $link . "\n\n";
					} else {
					    /** Insert record */
					    $data["url"] = $link;
						insertRec($data);
					}
				}
			}
		} else {
			echo "Could find links from " . $link . "\n\n";
		}
	}
	echo "Done.";
?>